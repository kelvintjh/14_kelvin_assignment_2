﻿using System;

namespace BowlingGame
{
    public class Game
    {
        int[] rollCount = new int[21];
        int rollCounter;
        int score = 0;
        int i = 0;

        public static void Main(string[] args)
        {

        }

        public void Roll(int pins)
        {
            rollCount[rollCounter] = pins;
            rollCounter++;
        }

        public void Strike()
        {
            score += 10 + rollCount[i + 1] + rollCount[i + 2];
            i += 1;
        }

        public void Spare()
        {
            score += 10 + rollCount[i + 2];
            i += 2;
        }

        public void Normal()
        {
            score += +rollCount[i] + rollCount[i + 1];
            i += 2;
        }

        public int Score()
        {
            for (int frame = 0; frame < 10; frame++)
            {
                if (rollCount[i] == 10)
                {
                    Strike();
                }
                else if (rollCount[i] + rollCount[i + 1] == 10)
                {
                    Spare();
                }
                else
                {
                    Normal();
                }
            }
            return score;

        }
    }
}
