﻿using NUnit.Framework;
using System;
using BowlingGame;

namespace BowlingTest
{
    [TestFixture()]
    public class Test
    {
        Game game;

        [SetUp]
        public void SetUp()
        {
            game = new Game();
        }

        public void RollMany(int rolls, int pins)
        {
            for (int i = 0; i < rolls; i++)
            {
                game.Roll(pins);
            }
        }
        
        //Test 0
        [Test()]
        public void CreateGame()
        {
            game = new Game();
        }

        //Test 1
        [Test]
        public void RollGutterAll()
        {
            RollMany(20, 0);
            Assert.That(game.Score(), Is.EqualTo(0));
        }

        //Test 2
        [Test]
        public void RollOnes()
        {
            RollMany(20, 1);
            Assert.That(game.Score(), Is.EqualTo(20));
        }

        //Test 3
        [Test]
        public void RollSpareFirstFrame()
        {
            game.Roll(9); game.Roll(1);
            RollMany(18, 1);
            Assert.That(game.Score(), Is.EqualTo(29));
        }

        //Test 4
        [Test]
        public void RollOneStrike()
        {
            game.Roll(10);
            RollMany(20, 1);
            Assert.That(game.Score(), Is.EqualTo(30));
        }

        //Test 5
        [Test]
        public void RollPerfectGame()
        {
            RollMany(12, 10);
            Assert.That(game.Score(), Is.EqualTo(300));
        }

        //Test 6
        [Test]
        public void RollAllSpares()
        {
            RollMany(21, 5);
            Assert.That(game.Score(), Is.EqualTo(150));
        }

        //Test 7
        [Test]
        public void RollStrikeHalfSpareHalf()
        {
            RollMany(10, 5);
            RollMany(6, 10);
            Assert.That(game.Score(), Is.EqualTo(220));
        }

        //Test 8
        [Test]
        public void TypicalGame()
        {
            game.Roll(10);
            game.Roll(9); game.Roll(1);
            game.Roll(5); game.Roll(5);
            game.Roll(7); game.Roll(2);
            game.Roll(10);
            game.Roll(10);
            game.Roll(10);
            game.Roll(9); game.Roll(0);
            game.Roll(8); game.Roll(2);
            game.Roll(9); game.Roll(1);
            game.Roll(10);
            Assert.That(game.Score(), Is.EqualTo(187));
        }
    }
}
